#ifndef CALLBACK_H
#define CALLBACK_H

#include <WString.h>
#include <Arduino.h>
#include <avr/eeprom.h>

#define PRM_LEN 6

const int buflen = 16;
const int refMagicNumber = 21391;


typedef void (*FunctionPointer)(String[]);

typedef struct  {
  String keyword;      // command (keyword) to match
  FunctionPointer fp;  // function reference
  int args;            // number of arguments
  String argDesc;
} CallBackDef;

class CallBack {
	public:
		CallBack(CallBackDef[], int,String,String, String, int, byte);
		void writeSettings(int, String, String, int, byte);
		void readSettings();
		unsigned int getNextEepromAddr();
		int	readMagic();
		void cmdCheck();
		void help();
		CallBackDef *myFuncs;
		void respond(String);
		void ok(); // respond ok
		void nok(String); // respond nok (not ok)
		void parseCmd(String);
		void run(int, int, String[]);
		int stoi(String);
		float stof(String);
		void setid(String);
		void setgid(String);
		void setdelay(String);
		void setecho(String);
		void printSettings();
		unsigned long lastSerialMillis;
		int noof; // number of functions
		String cmdBuf; // buffer for received command
		String myId; // identifier for specific device
		String myGid; // identifier for group of devices
		String myProgDescr; // description (used in help function)
		int myDelay; // response delay (ms) for group requests (to place responses in different time slots)
		byte myEcho; // command echo control
		int tmpDelay;
		String myArgv[PRM_LEN];
	
//		int magic;
//		int seenbefore;
};

#endif