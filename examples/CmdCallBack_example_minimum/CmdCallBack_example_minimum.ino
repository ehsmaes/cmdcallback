#include <CallBack.h>

// Compile and upload to arduino. Run the serial monitor and type command
// :help;

// Values for initiation of cmd/response interface. 
// After initial boot, id, gid and del are stored in eeprom.
// Change values by command. Make sure each device has a unique id.
String descr="Command/response test program v0.1";
String id="a1";
String gid="a";
int    del=0; //delayed response
byte   echo=1; // command back to host


// List of commands defined by keyword, funtion pointer, number of arguments 
// and description used in "help" command.
CallBackDef f[] = {
  {(String)"add",   (FunctionPointer)&add,  (int)2, (String)":num1:num2"}
};

// initiate command handler: function array, number of functions and intial values
CallBack cmd(f, sizeof(f) / sizeof(*f), id, gid, descr, del, echo);

void setup() {
  Serial.begin(9600);
  cmd.ok(); // say hello
}

void loop() {
  // Don't forget this line. Parse command if serial data is available.
  cmd.cmdCheck();

  // Put code here. Use timers instead of delay if possible as not to disrupt
  // command/response interaction with host


}


//   --------- command initiated callback functions below ---------
// callback functions all need to be defined void and with String argv
// argument list. The command parser will validate the number of input
// parameters but any additional validation has to be perfomed by each
// callback function. As the argument list is passed as strings, type
// casting to other types is the responsibility of the function.


void add(String argv[]) {
  int a = cmd.stoi(argv[0]);
  int b = cmd.stoi(argv[1]);
  cmd.respond(String(a + b));
}
